package incrocio_grafica;


class Incrocio {
    int N=0,E=0,S=0,W=0;

    public synchronized void checkRight(int s) throws InterruptedException {
        if(s==1){
            while(W!=0){
                wait();
            }
        }
        else if(s==2){
            while(N!=0){
                wait();
            }
        }
        if(s==3){
            while(E!=0){
                wait();
            }
        }
        if(s==4){
            while(S!=0){
                wait();
            }
        }
    }

    public synchronized void occupa(int s) {
        if(s==1)
            N++;
        else if(s==2)
            E++;
        else if(s==3)
            S++;
        else
            W++;
    }

    public synchronized void libera(int s) {
        if(s==1)
            N--;
        else if(s==2)
            E--;
        else if(s==3)
            S--;
        else
            W--;
        notifyAll();
    }
}
