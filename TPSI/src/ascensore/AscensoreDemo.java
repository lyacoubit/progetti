package ascensore;

import javax.swing.*;

class AscensoreDemo extends JFrame{
    AscensoreDemo(){
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(0,0,500,600);
        setVisible(true);

        Ascensore a=new Ascensore();
        GUI g=new GUI(a);

        add(g);

        a.start();
    }

    public static void main(String args[]){
        new AscensoreDemo();
    }
}
