
package ascensore;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.Timer;

class GUI extends JPanel {
    Listener l;
    JButton b1;
    JButton b2;
    JButton b3;
    Ascensore a;

    GUI(Ascensore a) {
        this.a = a;
        this.b1 = new JButton();
        this.b2 = new JButton();
        this.b3 = new JButton();
        this.l = new Listener(this.b1, this.b2, this.b3, a);
        this.b1.setText("1");
        this.b2.setText("2");
        this.b3.setText("3");
        this.b1.setBounds(0, 0, 30, 10);
        this.b2.setBounds(0, 10, 30, 10);
        this.b3.setBounds(0, 20, 30, 10);
        this.b1.addActionListener(this.l);
        this.b2.addActionListener(this.l);
        this.b3.addActionListener(this.l);
        this.add(this.b1);
        this.add(this.b2);
        this.add(this.b3);
        Timer t = new Timer(50, new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                GUI.this.repaint();
            }
        });
        t.setRepeats(true);
        t.start();
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setColor(Color.BLACK);
        g2.drawRect(200, 50, 100, 450);
        g2.drawRect(this.a.getX1(), this.a.getY1(), this.a.getX(), this.a.getY());
    }
}