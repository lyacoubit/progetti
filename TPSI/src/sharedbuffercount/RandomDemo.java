package sharedbuffercount;

class RandomDemo {
    public static void main(String args[]){
        SharedZone s=new SharedZone();
        Writer w=new Writer(s);
        Reader r=new Reader(s);

        w.start();
        r.start();
    }
}
