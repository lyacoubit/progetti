package sharedbuffercount;

class Writer extends Thread{
    SharedZone s;
    int n;

    Writer(SharedZone s){
        this.s=s;
    }

    @Override
    public void run() {
        super.run();
        while(true) {
            n = (int) (Math.random() * 1000);
            s.setValue(n);
            try{
                this.sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
