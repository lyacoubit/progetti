package ponte;


class Ponte {
    boolean occupied=false;

    synchronized void entra() {
        while(occupied){
            try{
                System.out.println("La macchina "+Thread.currentThread().getName()+" si ferma");
                wait();
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println("La macchina "+Thread.currentThread().getName()+" entra nel ponte");
        occupied=true;
    }

    synchronized void esce() {
        occupied=false;
        notifyAll();
    }
}
