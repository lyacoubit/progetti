package ponte;

class Macchina extends Thread{
    Ponte p;
    int dist=0;

    Macchina(Ponte p) {
        this.p=p;
    }

    @Override
    public void run() {
        super.run();
        while(dist!=100){
            try{
                Thread.sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            dist+=10;
            if(dist==50)
                p.entra();
            if(dist==60)
                p.esce();
            System.out.println(Thread.currentThread().getName()+" ha percorso "+dist+"m");
        }
        System.out.println(Thread.currentThread().getName()+" e' arrivata.");
    }
}
