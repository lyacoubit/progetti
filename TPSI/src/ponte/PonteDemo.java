package ponte;


class PonteDemo {
    public static void main(String args[]){
        Ponte p=new Ponte();
        Macchina m1=new Macchina(p);
        Macchina m2=new Macchina(p);
        Macchina m3=new Macchina(p);

        m1.setName("M1");
        m2.setName("M2");
        m3.setName("M3");

        m1.start();
        m2.start();
        m3.start();
    }
}
