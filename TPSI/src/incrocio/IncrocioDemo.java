package incrocio;

import javax.swing.*;


class IncrocioDemo extends JFrame{

    IncrocioDemo(){
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(400, 100, 560, 560);
        setResizable(false);
        setVisible(true);

        Incrocio i=new Incrocio();

        Macchina m1=new Macchina(i);
        Macchina m2=new Macchina(i);
        Macchina m3=new Macchina(i);
        Macchina m4=new Macchina(i);

        m1.setName("M1");
        m2.setName("M2");
        m3.setName("M3");
        m4.setName("M4");

        m1.start();
        m2.start();
        m3.start();
        m4.start();
    }

    public static void main(String args[]){
        new IncrocioDemo();
    }
}
