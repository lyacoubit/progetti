package incrocio;

/**
 * Created by simone on 30/10/16.
 */
class Macchina extends Thread{
    int dist;
    Incrocio i;

    Macchina(Incrocio i){
        this.i=i;
    }

    public int getDist() {
        return dist;
    }

    @Override
    public void run() {
        super.run();
        while(dist!=10){
            try{
                Thread.sleep(1100);
            }catch (InterruptedException w){
                w.printStackTrace();
            }
            dist+=1;
            System.out.println(Thread.currentThread().getName()+" ha percorso "+dist+"m");
            if(dist==5)
                try {
                    i.occupa();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            if(dist==6)
                try {
                    i.libera();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        }
        System.out.println(Thread.currentThread().getName()+" e' arrivata a destinazione");
    }
}
