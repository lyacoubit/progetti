package incrocio;


class Incrocio {
    boolean occupied=false;

    synchronized void occupa() throws InterruptedException {
        while(occupied){
            System.out.println(Thread.currentThread().getName()+" si ferma all'incrocio");
            wait();
        }
        System.out.println(Thread.currentThread().getName()+" occupa l'incrocio");
        occupied=true;
    }

    synchronized void libera() throws InterruptedException {
        occupied=false;
        notifyAll();
    }
}
