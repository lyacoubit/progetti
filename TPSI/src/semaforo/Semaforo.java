package semaforo;
class Semaforo extends Thread{
    private int RED=5000;
    private int YELLOW=1000;
    private int GREEN=2000;
    private int stato;

    Semaforo(){}

    @Override
    public void run() {
        super.run();
        while (Macchina.posizione!=100)
            try{
                stato=YELLOW;
                System.out.println("Giallo");
                Thread.sleep(stato);
                stato=RED;
                System.out.println("Rosso");
                Thread.sleep(stato);
                stato=GREEN;
                System.out.println("Verde");
                setStato();
                Thread.sleep(stato);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
    }

    synchronized void attendiVerde(){
        while(stato==RED){
            System.out.println("La macchina si ferma");
            try {
                wait();
               
            }catch (InterruptedException w){
                w.printStackTrace();
            }
        }
    }

    synchronized void setStato(){
        while(stato==RED){
        }
        notifyAll();
        System.out.println("La macchina riparte");
    }
}
