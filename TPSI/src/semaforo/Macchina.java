package semaforo;

public class  Macchina extends Thread {
    Semaforo s;
    static int posizione = 0;
    private long t;

    Macchina(Semaforo s) {
        this.s = s;
    }

    @Override
    public void run() {
        super.run();
        t = System.currentTimeMillis();
        while (posizione != 100) {
            try {
            	 posizione += 10;
                  System.out.println("Posizione= " + posizione);
                  
            if(posizione==50) {
                s.attendiVerde();  }
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace(); 
           }
            if (posizione == 100)
                System.out.println("Tempo impiegato " + (System.currentTimeMillis() - t) + "; posizione " + posizione);
        }
    }
}


